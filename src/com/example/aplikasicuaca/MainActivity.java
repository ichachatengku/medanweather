package com.example.aplikasicuaca;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends Activity {

	private String url1 = "http://api.openweathermap.org/data/2.5/weather?q=Medan&appid=fd4895632dba7c7982fafb8b82ae2ad6";
	private ImageView gambar;
	private TextView pesan;
	private HandleJSON obj;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		gambar = (ImageView) findViewById(R.id.gambarCuaca);
		pesan = (TextView) findViewById(R.id.message);
		TextView temperature = (TextView)findViewById(R.id.suhu);
		TextView jam = (TextView)findViewById(R.id.jam);
		TextView tanggal = (TextView)findViewById(R.id.tanggal);

		String finalUrl = url1;
		obj = new HandleJSON(finalUrl);
		obj.fetchJSON();

		while(obj.parsingComplete);
		temperature.setText(String.valueOf(obj.getTemperature()));
		jam.setText(getTime());
		tanggal.setText(getDate());
		Imageicon(obj.getIcon());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public static String getDate()
	{
		SimpleDateFormat date = new SimpleDateFormat("EEE, MMM dd");
		String strdate = date.format(Calendar.getInstance().getTime());
		return strdate;
	}
	
	public static String getTime()
	{
		SimpleDateFormat time = new SimpleDateFormat("hh:mm a");
		String strtime = time.format(Calendar.getInstance().getTime());
		return strtime;
	}
	
	private void Imageicon (String image){
		if(image.equals("01d")){
			//clear sky
			gambar.setImageResource(R.drawable.clear);
			pesan.setText(String.valueOf("Cuaca hari ini cerah sekali! Jangan lupa pakai sunblock"));
		}
		else if (image.equals("01n")){
			//clear night sky
			gambar.setImageResource(R.drawable.ntclear);
			pesan.setText(String.valueOf("Langit malam sedang cerah! Lihatlah indahnya bulan dan bintang di langit"));
		}
		else if (image.equals("02d")){
			//few clouds
			gambar.setImageResource(R.drawable.mostlysunny);
			pesan.setText(String.valueOf("Cuaca hari ini cerah, namun matahari tidak terlalu terik. Saat yang tepat untuk jalan-jalan"));
		}
		else if (image.equals("02n")){
			//few clouds in the night
			gambar.setImageResource(R.drawable.ntmostlycloudy);
			pesan.setText(String.valueOf("Malam ini cerah berawan. Saat yang tepat menikmati angin malam"));
		}
		else if (image.equals("03d") ||image.equals("03n") ){
			//berawan
			gambar.setImageResource(R.drawable.cloudy);
			pesan.setText(String.valueOf("Langit berawan. Jika ingin berpergian, jangan terlalu lama. Cuaca bisa saja berubah setiap saat"));
		}
		else if (image.equals("04d") || image.equals("04n") ){
			//mendung
			gambar.setImageResource(R.drawable.fog);
			pesan.setText(String.valueOf("Wah mendung nih. Jangan lupa sedia payung jika harus berpergian. Being at home is better"));
		}
		else if (image.equals("09d") || image.equals("10d") || image.equals("09n") || image.equals("10n")){
			//hujan
			gambar.setImageResource(R.drawable.chancerain);
			pesan.setText(String.valueOf("Wah hujan! Cancel dulu deh jalan-jalannya~"));
		}
		else if (image.equals("11d") || image.equals("11n")){
			//hujan lebat
			gambar.setImageResource(R.drawable.chancetstorms);
			pesan.setText(String.valueOf("Hujan lebat! Beruntung sekali sedang di rumah"));
		}
		else if (image.equals("13d") || image.equals("13n")){
			//badaai~
			gambar.setImageResource(R.drawable.chanceflurries);
			pesan.setText(String.valueOf("Ya ampun hujan lebat dan petir!! Semoga tidak disertai mati lampu"));
		}

	}
	
	

}
